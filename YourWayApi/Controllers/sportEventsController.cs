﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using YourWayApi.Models;

namespace YourWayApi.Controllers
{
    [RoutePrefix("api/sportEvents")]
    public class sportEventsController : ApiController
    {
        private yourwayEntities db = new yourwayEntities();

        // GET: api/sportEvents
        public IQueryable<sportEvent> GetsportEvents()
        {
            return db.sportEvents.Where(s => s.active);
        }

        // GET: api/sportEvents/user/{userId:int}
        [HttpGet]
        [Route("user/{userId:int}")]
        public IQueryable<sportEvent> GetsportEventsByUser(int userId)
        {
            return db.sportEvents.Where(s => s.userId == userId && s.active);
        }

        // GET: api/sportEvents/asmember/{userId:int}
        [HttpGet]
        [Route("asmember/{userId:int}")]
        public IQueryable<sportEvent> GetsportEventsAsMember(int userId)
        {
            return db.sportEventByUsers.Where(s => s.userId == userId).Select(se => se.sportEvent);
        }

        // GET: api/sportEvents/{id:int}/rate/{rating:float}
        [HttpPut]
        [Route("{id:int}/rate/{rating:float}")]
        [ResponseType(typeof(sportEvent))]
        public IHttpActionResult PutRating(int id, float rating)
        {
            var se = db.sportEvents.Find(id);
            if (se == null)
            {
                return NotFound();
            }
            if (!se.active)
            {
                return NotFound();
            }

            se.rating = rating;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!sportEventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(se);
        }

        // GET: api/sportEvents/5
        [ResponseType(typeof(sportEvent))]
        public IHttpActionResult GetsportEvent(int id)
        {
            sportEvent sportEvent = db.sportEvents.Find(id);
            if (sportEvent == null)
            {
                return NotFound();
            }
            if (!sportEvent.active)
            {
                return NotFound();
            }

            return Ok(sportEvent);
        }

        // PUT: api/sportEvents/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutsportEvent(int id, sportEvent sportEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sportEvent.id)
            {
                return BadRequest();
            }

            db.Entry(sportEvent).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!sportEventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/sportEvents
        [ResponseType(typeof(sportEvent))]
        public IHttpActionResult PostsportEvent(sportEvent sportEvent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //agrego el evento
            var newEvent = db.sportEvents.Add(sportEvent);
            db.SaveChanges();

            return Ok(newEvent);
        }

        // DELETE: api/sportEvents/5
        [ResponseType(typeof(sportEvent))]
        public IHttpActionResult DeletesportEvent(int id)
        {
            sportEvent sportEvent = db.sportEvents.Find(id);
            if (sportEvent == null)
            {
                return NotFound();
            }

            sportEvent.active = false;

            var sebus = db.sportEventByUsers.Where(s => s.eventId == id);
            db.sportEventByUsers.RemoveRange(sebus);
            db.SaveChanges();

            return Ok(sportEvent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool sportEventExists(int id)
        {
            return db.sportEvents.Count(e => e.id == id) > 0;
        }
    }
}