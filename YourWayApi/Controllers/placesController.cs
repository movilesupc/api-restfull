﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using YourWayApi.Models;

namespace YourWayApi.Controllers
{
    [RoutePrefix("api/places")]
    public class placesController : ApiController
    {
        private yourwayEntities db = new yourwayEntities();

        // GET: api/places
        public IQueryable<place> Getplaces()
        {
            return db.places.Where(p => p.active);
        }

        // GET: api/places/sportType/{sportTypeId:int}
        [HttpGet]
        [Route("sportType/{sportTypeId:int}")]
        public IQueryable<place> GetplacesBySportType(int sportTypeId)
        {
            return db.places.Where(p => p.sportTypeId == sportTypeId && p.active);
        }

        // GET: api/places/5
        [ResponseType(typeof(place))]
        public IHttpActionResult Getplace(int id)
        {
            place place = db.places.Find(id);
            if (place == null)
            {
                return NotFound();
            }

            return Ok(place);
        }

        // PUT: api/places/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putplace(int id, place place)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != place.id)
            {
                return BadRequest();
            }

            db.Entry(place).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!placeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/places
        [ResponseType(typeof(place))]
        public IHttpActionResult Postplace(place place)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.places.Add(place);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = place.id }, place);
        }

        // DELETE: api/places/5
        [ResponseType(typeof(place))]
        public IHttpActionResult Deleteplace(int id)
        {
            place place = db.places.Find(id);
            if (place == null)
            {
                return NotFound();
            }

            place.active = false;
            db.SaveChanges();

            return Ok(place);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool placeExists(int id)
        {
            return db.places.Count(e => e.id == id) > 0;
        }
    }
}