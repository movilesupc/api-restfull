﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using YourWayApi.Models;

namespace YourWayApi.Controllers
{
    [RoutePrefix("api/users")]
    public class usersController : ApiController
    {
        private yourwayEntities db = new yourwayEntities();

        // GET: api/users
        public IQueryable<user> Getusers()
        {
            return db.users.Where(u => u.active);
        }

        // GET: api/users/5
        [ResponseType(typeof(user))]
        public IHttpActionResult Getuser(int id)
        {
            user user = db.users.Find(id);
            if (user == null)
            {
                return NotFound();
            }
            if (user.active == false)
            {
                return NotFound();
            }

            return Ok(new { user.id, user.name, user.lastName, user.email, user.imageUrl });
        }

        // GET: api/users/{id:int}/friends
        [HttpGet]
        [Route("{id:int}/friends")]
        public IQueryable<user> GetusersFriends(int id)
        {
            return db.friendByUsers.Where(u => u.active && u.userId == id).Select(us => us.user1);
        }

        // POST: api/users/{id:int}/friends
        [HttpPost]
        [Route("{id:int}/friends")]
        public IHttpActionResult PostUsersFriends(friendByUser friend)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var f = db.friendByUsers.Add(friend);
            db.SaveChanges();
            return Ok(f);
        }

        // DELETE: api/users/{id:int}/friends
        [HttpDelete]
        [Route("{id:int}/friends/{friendId:int}")]
        public IHttpActionResult DeleteUsersFriends(int id, int friendId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var friend = db.friendByUsers.FirstOrDefault(f => f.userId == id && f.friendId == friendId);

            if (friend == null)
            {
                return NotFound();
            }

            friend.active = false;
            db.SaveChanges();
            return Ok(friend);
        }

        // PUT: api/users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putuser(int id, user user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.id)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!userExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/users
        [ResponseType(typeof(user))]
        public IHttpActionResult Postuser(user user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!userExists(user.id))
            {
                db.users.Add(user);
                db.SaveChanges();
                return Ok(new { user.id, user.name, user.lastName, user.email, user.imageUrl });
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE: api/users/5
        [ResponseType(typeof(user))]
        public IHttpActionResult Deleteuser(int id)
        {
            user user = db.users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            user.active = false;
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool userExists(int id)
        {
            return db.users.Count(e => e.id == id) > 0;
        }
    }
}