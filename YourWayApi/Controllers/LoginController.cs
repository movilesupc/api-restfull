﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using YourWayApi.Models;
using YourWayApi.Models.DTO;

namespace YourWayApi.Controllers
{
    public class LoginController : ApiController
    {
        private yourwayEntities db = new yourwayEntities();

        // POST: api/Login
        [ResponseType(typeof(user))]
        public IHttpActionResult Post([FromBody]LoginDTO login)
        {
            user user = db.users.FirstOrDefault(u => u.email == login.email && u.password == login.password);
            if (user == null)
            {
                return NotFound();
            }
            if (user.active == false)
            {
                return NotFound();
            }

            return Ok(new { user.id, user.name, user.lastName, user.email, user.imageUrl });
        }
    }
}
