using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class SportsmanService : ISportsmanService
    {
        private ISportsmanRepository sportsmanRepository;
        public SportsmanService(ISportsmanRepository sportsmanRepository){
            this.sportsmanRepository = sportsmanRepository;
        }


        public bool Create(Sportsman entity)
        {
            return sportsmanRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Sportsman> FindAll()
        {
            return sportsmanRepository.FindAll();
        }

        public Sportsman FindById(int id)
        {
            return sportsmanRepository.FindById(id);
        }

        public bool Update(Sportsman entity)
        {
            return sportsmanRepository.Update(entity);
        }
    }
}