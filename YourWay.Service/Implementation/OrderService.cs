using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class OrderService : IOrderService
    {
        private IOrderRepository orderRepository;
        public OrderService(IOrderRepository orderRepository){
            this.orderRepository = orderRepository;
        }


        public bool Create(Order entity)
        {
            return orderRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Order> FindAll()
        {
            return orderRepository.FindAll();
        }

        public Order FindById(int id)
        {
            return orderRepository.FindById(id);
        }

        public bool Update(Order entity)
        {
            throw new System.NotImplementedException();
        }
    }
}