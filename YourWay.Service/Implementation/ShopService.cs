using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class ShopService : IShopService
    {
        private IShopRepository shopRepository;
        public ShopService(IShopRepository shopRepository){
            this.shopRepository = shopRepository;
        }


        public bool Create(Shop entity)
        {
            return shopRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Shop> FindAll()
        {
            return shopRepository.FindAll();
        }

        public Shop FindById(int id)
        {
            return shopRepository.FindById(id);
        }

        public bool Update(Shop entity)
        {
            throw new System.NotImplementedException();
        }
    }
}