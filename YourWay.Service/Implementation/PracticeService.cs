using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class PracticeService : IPracticeService
    {
        private IPracticeRepository practiceRepository;
        public PracticeService(IPracticeRepository practiceRepository){
            this.practiceRepository = practiceRepository;
        }


        public bool Create(Practice entity)
        {
            return practiceRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Practice> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public Practice FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Practice entity)
        {
            throw new System.NotImplementedException();
        }
    }
}