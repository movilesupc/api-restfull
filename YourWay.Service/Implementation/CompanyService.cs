using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class CompanyService : ICompanyService
    {
        private ICompanyRepository companyRepository;
        public CompanyService(ICompanyRepository companyRepository){
            this.companyRepository = companyRepository;
        }

        
        public bool Create(Company entity)
        {
            return companyRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Company> FindAll()
        {
            return companyRepository.FindAll();
        }

        public Company FindById(int id)
        {
            return companyRepository.FindById(id);
        }

        public bool Update(Company entity)
        {
            return companyRepository.Update(entity);
        }
    }
}