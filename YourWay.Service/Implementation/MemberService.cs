using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class MemberService : IMemberService
    {
        private IMemberRepository memberRepository;
        public MemberService(IMemberRepository memberRepository){
            this.memberRepository = memberRepository;
        }


        public bool Create(Member entity)
        {
            return memberRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Member> FindAll()
        {
            return memberRepository.FindAll();
        }

        public Member FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Member entity)
        {
            throw new System.NotImplementedException();
        }
    }
}