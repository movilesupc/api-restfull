using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class GroupSportService : IGroupSportService
    {
        private IGroupSportRepository groupSportRepository;
        public GroupSportService(IGroupSportRepository groupSportRepository){
            this.groupSportRepository = groupSportRepository;
        }


        public bool Create(GroupSport entity)
        {
            return groupSportRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<GroupSport> FindAll()
        {
            return groupSportRepository.FindAll();
        }

        public GroupSport FindById(int id)
        {
            return groupSportRepository.FindById(id);
        }

        public bool Update(GroupSport entity)
        {
            throw new System.NotImplementedException();
        }
    }
}