using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class CompetitorService : ICompetitorService
    {
        private ICompetitorRepository competitorRepository;
        public CompetitorService(ICompetitorRepository competitorRepository){
            this.competitorRepository = competitorRepository;
        }


        public bool Create(Competitor entity)
        {
            return competitorRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Competitor> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public Competitor FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Competitor entity)
        {
            throw new System.NotImplementedException();
        }
    }
}