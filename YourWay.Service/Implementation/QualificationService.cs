using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class QualificationService : IQualificationService
    {
        private IQualificationRepository qualificationRepository;
        public QualificationService(IQualificationRepository qualificationRepository){
            this.qualificationRepository = qualificationRepository;
        }


        public bool Create(Qualification entity)
        {
            return qualificationRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Qualification> FindAll()
        {
            return qualificationRepository.FindAll();
        }

        public Qualification FindById(int id)
        {
            return qualificationRepository.FindById(id);
        }

        public bool Update(Qualification entity)
        {
            throw new System.NotImplementedException();
        }
    }
}