using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class SportService : ISportService
    {
        private ISportRepository sportRepository;
        public SportService(ISportRepository sportRepository){
            this.sportRepository = sportRepository;
        }


        public bool Create(Sport entity)
        {
            return sportRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Sport> FindAll()
        {
            return sportRepository.FindAll();
        }

        public Sport FindById(int id)
        {
            return sportRepository.FindById(id);
        }

        public bool Update(Sport entity)
        {
            throw new System.NotImplementedException();
        }
    }
}