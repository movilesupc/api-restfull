using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class ProductService : IProductService
    {
        private IProductRepository productRepository;
        public ProductService(IProductRepository productRepository){
            this.productRepository = productRepository;
        }


        public bool Create(Product entity)
        {
            return productRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Product> FindAll()
        {
            return productRepository.FindAll();
        }

        public Product FindById(int id)
        {
            return productRepository.FindById(id);
        }

        public bool Update(Product entity)
        {
            throw new System.NotImplementedException();
        }
    }
}