using System.Collections.Generic;
using YourWay.Entity;
using YourWay.Repository.Interface;
using YourWay.Service.Interface;

namespace YourWay.Service.Implementation
{
    public class EventSportService : IEventSportService
    {
        private IEventSportRepository eventSportRepository;
        public EventSportService(IEventSportRepository eventSportRepository){
            this.eventSportRepository = eventSportRepository;
        }


        public bool Create(EventSport entity)
        {
            return eventSportRepository.Create(entity);
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<EventSport> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public EventSport FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(EventSport entity)
        {
            throw new System.NotImplementedException();
        }
    }
}