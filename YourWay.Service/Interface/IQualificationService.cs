using YourWay.Entity;

namespace YourWay.Service.Interface
{
    public interface IQualificationService: ICrudService<Qualification>
    {
         
    }
}