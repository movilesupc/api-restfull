using YourWay.Entity;

namespace YourWay.Service.Interface
{
    public interface IOrderProductService: ICrudService<OrderProduct>
    {
         
    }
}