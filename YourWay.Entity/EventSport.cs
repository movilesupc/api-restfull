namespace YourWay.Entity
{
    public class EventSport
    {
        public int Id {get; set;}

        public int EventId {get; set;}
        public Event Event {get; set;}
        public int SportId {get; set;}
        public Sport Sport {get; set;}
    }
}