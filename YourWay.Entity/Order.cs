namespace YourWay.Entity
{
    public class Order
    {
        public int Id {get; set;}
        public float TotalPrice {get; set;}

        public int SportsManId {get; set;}
        public Sportsman SportsMan {get; set;}
        public int LocationId {get; set;}
        public Location Location {get; set;}
    }
}