namespace YourWay.Entity
{
    public class GroupSport
    {
        public int Id {get; set;}

        public int GroupId {get; set;}
        public Group Group {get; set;}
        public int SportId {get; set;}
        public Sport Sport {get; set;}
    }
}