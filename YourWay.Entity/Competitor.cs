namespace YourWay.Entity
{
    public class Competitor
    {
        public int Id {get; set;}
        public bool ValidateInvitation {get; set;}
        public bool ValidateSolicitude {get; set;}

        
        public int EventId {get; set;}
        public Event Event {get; set;}
        public int SportManId {get; set;}
        public Sportsman Sportsman {get; set;}
    }
}