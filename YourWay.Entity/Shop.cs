namespace YourWay.Entity
{
    public class Shop
    {
        public int Id {get; set;}
        public string Name {get; set;}

        public int LocationId {get; set;}
        public Location Location {get; set;}
        public int CompanyId {get; set;}
        public Company Company {get; set;}
    }
}