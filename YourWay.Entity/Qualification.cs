namespace YourWay.Entity
{
    public class Qualification
    {
        public int Id {get; set;}
        public string Comment {get; set;}
        public int Score {get; set;}

        public int CompetitorId {get; set;}
        public Competitor Competitor {get; set;}
    }
}