namespace YourWay.Entity
{
    public class Member
    {
        public int Id {get; set;}

        public int SportsManId {get; set;}
        public Sportsman SportsMan {get; set;}
        public int GroupId {get; set;}
        public Group Group {get; set;}
    }
}