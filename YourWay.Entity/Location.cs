namespace YourWay.Entity
{
    public class Location
    {
        public int Id {get; set;}
        public string Place {get; set;}
        public double Latitude {get; set;}
        public double Altitude {get; set;}
    }
}