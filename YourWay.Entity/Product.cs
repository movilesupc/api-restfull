namespace YourWay.Entity
{
    public class Product
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public string Photo {get; set;}
        public float Price {get; set;}

        public int ShopId {get; set;}
        public Shop Shop {get; set;}
        public int CategoryId {get; set;}
        public Category Category {get; set;}
    }
}