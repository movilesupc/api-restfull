using System;

namespace YourWay.Entity
{
    public class Event
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public int Capacity {get; set;}
        public DateTime StartDate {get; set;}
        public DateTime EndDate {get; set;}
        public double Duration {get; set;}

        public int LocationId {get; set;}
        public Location Location {get; set;}
        public int SportsManId {get; set;}
        public Sportsman SportsMan {get; set;}

    }
}