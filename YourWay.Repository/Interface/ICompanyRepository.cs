using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface ICompanyRepository: ICrudRepository<Company>
    {
         
    }
}