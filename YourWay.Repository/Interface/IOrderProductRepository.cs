using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IOrderProductRepository: ICrudRepository<OrderProduct>
    {
         
    }
}