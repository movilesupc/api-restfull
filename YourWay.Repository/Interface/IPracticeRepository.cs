using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IPracticeRepository: ICrudRepository<Practice>
    {
         
    }
}