using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface ISportRepository: ICrudRepository<Sport>
    {
         
    }
}