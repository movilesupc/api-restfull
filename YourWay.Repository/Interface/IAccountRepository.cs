using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IAccountRepository: ICrudRepository<Account>
    {
         
    }
}