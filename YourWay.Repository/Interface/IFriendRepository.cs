using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IFriendRepository: ICrudRepository<Friend>
    {
         
    }
}