using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IEventRepository: ICrudRepository<Event>
    {
         
    }
}