using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IProductRepository: ICrudRepository<Product>
    {
         
    }
}