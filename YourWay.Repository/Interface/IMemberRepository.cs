using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IMemberRepository: ICrudRepository<Member>
    {
         
    }
}