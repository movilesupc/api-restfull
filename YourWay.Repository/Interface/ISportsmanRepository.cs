using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface ISportsmanRepository: ICrudRepository<Sportsman>
    {
         
    }
}