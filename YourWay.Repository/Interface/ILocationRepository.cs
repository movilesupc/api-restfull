using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface ILocationRepository: ICrudRepository<Location>
    {
         
    }
}