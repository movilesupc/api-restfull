using YourWay.Entity;

namespace YourWay.Repository.Interface
{
    public interface IQualificationRepository: ICrudRepository<Qualification>
    {
         
    }
}