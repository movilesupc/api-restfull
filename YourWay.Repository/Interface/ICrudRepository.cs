using System.Collections.Generic;

namespace YourWay.Repository.Interface
{
    public interface ICrudRepository<T>
    {
        bool Create(T entity);
        bool Update(T entity);
        bool Delete(int id);
        IEnumerable<T> FindAll();
        T FindById(int id);
    }
}