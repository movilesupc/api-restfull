﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace YourWay.Repository.Migrations
{
    public partial class db_inicial_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "OrderProduct",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<float>(
                name: "SubPrice",
                table: "OrderProduct",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "OrderProduct");

            migrationBuilder.DropColumn(
                name: "SubPrice",
                table: "OrderProduct");
        }
    }
}
