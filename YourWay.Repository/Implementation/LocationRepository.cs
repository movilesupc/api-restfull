using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class LocationRepository : ILocationRepository
    {
        private ApplicationDbContext context;
        public LocationRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Location entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Location> FindAll()
        {
            var resultado = new List<Location>();
            
            try{
                resultado = context.Location.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Location FindById(int id)
        {
            var resultado = new Location();

            try{
                resultado = context.Location.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Location entity)
        {
            try{
                var LocationOriginal = context.Location.Single(p => p.Id == entity.Id);

                LocationOriginal.Id = entity.Id;
                LocationOriginal.Altitude = entity.Altitude;
                LocationOriginal.Latitude = entity.Latitude;
                LocationOriginal.Place = entity.Place;

                context.Update(LocationOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }
    }
}