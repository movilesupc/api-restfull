using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class OrderRepository : IOrderRepository
    {
        private ApplicationDbContext context;
        public OrderRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Order entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Order> FindAll()
        {
            var resultado = new List<Order>();
            
            try{
                resultado = context.Order.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Order FindById(int id)
        {
            var resultado = new Order();

            try{
                resultado = context.Order.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Order entity)
        {
            throw new System.NotImplementedException();
        }
    }
}