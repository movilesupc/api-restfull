using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class FriendRepository : IFriendRepository
    {
        private ApplicationDbContext context;
        public FriendRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Friend entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Friend> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public Friend FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Friend entity)
        {
            throw new System.NotImplementedException();
        }
    }
}