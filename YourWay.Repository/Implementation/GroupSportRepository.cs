using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class GroupSportRepository : IGroupSportRepository
    {
        private ApplicationDbContext context;
        public GroupSportRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(GroupSport entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<GroupSport> FindAll()
        {
            var resultado = new List<GroupSport>();
            
            try{
                resultado = context.GroupSport.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public GroupSport FindById(int id)
        {
            var resultado = new GroupSport();

            try{
                resultado = context.GroupSport.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(GroupSport entity)
        {
            throw new System.NotImplementedException();
        }
    }
}