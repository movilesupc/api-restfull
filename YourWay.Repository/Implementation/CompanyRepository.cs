using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class CompanyRepository : ICompanyRepository
    {
        private ApplicationDbContext context;
        public CompanyRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Company entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Company> FindAll()
        {
            var resultado = new List<Company>();
            
            try{
                resultado = context.Company.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Company FindById(int id)
        {
            var resultado = new Company();

            try{
                resultado = context.Company.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Company entity)
        {
            throw new System.NotImplementedException();
        }
    }
}