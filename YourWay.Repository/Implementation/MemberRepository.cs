using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class MemberRepository : IMemberRepository
    {
        private ApplicationDbContext context;
        public MemberRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Member entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Member> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public Member FindById(int id)
        {
            var resultado = new Member();

            try{
                resultado = context.Member.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Member entity)
        {
            throw new System.NotImplementedException();
        }
    }
}