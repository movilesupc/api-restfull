using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class EventRepository : IEventRepository
    {
        private ApplicationDbContext context;
        public EventRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Event entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Event> FindAll()
        {
            var resultado = new List<Event>();
            
            try{
                resultado = context.Event.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Event FindById(int id)
        {
            var resultado = new Event();

            try{
                resultado = context.Event.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Event entity)
        {
            try{
                var EventOriginal = context.Event.Single(p => p.Id == entity.Id);

                EventOriginal.Id = entity.Id;
                EventOriginal.Name = entity.Name;
                EventOriginal.Capacity = entity.Capacity;
                EventOriginal.Duration = entity.Duration;
                EventOriginal.Duration = entity.Duration;
                EventOriginal.StartDate = entity.StartDate;
                EventOriginal.EndDate = entity.EndDate;

                EventOriginal.SportsManId = entity.SportsManId;
                EventOriginal.LocationId = entity.LocationId;

                context.Update(EventOriginal);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }
    }
}