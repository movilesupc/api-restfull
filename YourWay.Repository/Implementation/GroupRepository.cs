using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class GroupRepository : IGroupRepository
    {
        private ApplicationDbContext context;
        public GroupRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Group entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Group> FindAll()
        {
            var resultado = new List<Group>();
            
            try{
                resultado = context.Group.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Group FindById(int id)
        {
            var resultado = new Group();

            try{
                resultado = context.Group.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Group entity)
        {
            throw new System.NotImplementedException();
        }
    }
}