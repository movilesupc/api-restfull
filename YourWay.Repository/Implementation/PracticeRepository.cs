using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class PracticeRepository : IPracticeRepository
    {
        private ApplicationDbContext context;
        public PracticeRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Practice entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Practice> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public Practice FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Practice entity)
        {
            throw new System.NotImplementedException();
        }
    }
}