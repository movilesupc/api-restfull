using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class CompetitorRepository : ICompetitorRepository
    {
        private ApplicationDbContext context;
        public CompetitorRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Competitor entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Competitor> FindAll()
        {
            throw new System.NotImplementedException();
        }

        public Competitor FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Competitor entity)
        {
            throw new System.NotImplementedException();
        }
    }
}