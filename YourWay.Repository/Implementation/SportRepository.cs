using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class SportRepository : ISportRepository
    {
        private ApplicationDbContext context;
        public SportRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Sport entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Sport> FindAll()
        {
            var resultado = new List<Sport>();
            
            try{
                resultado = context.Sport.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Sport FindById(int id)
        {
            var resultado = new Sport();

            try{
                resultado = context.Sport.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Sport entity)
        {
            throw new System.NotImplementedException();
        }
    }
}