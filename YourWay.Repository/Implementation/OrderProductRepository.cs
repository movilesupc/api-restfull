using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class OrderProductRepository : IOrderProductRepository
    {
        private ApplicationDbContext context;
        public OrderProductRepository(ApplicationDbContext context){
            this.context = context;
        }

        
        public bool Create(OrderProduct entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<OrderProduct> FindAll()
        {
            var resultado = new List<OrderProduct>();
            
            try{
                resultado = context.OrderProduct.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public OrderProduct FindById(int id)
        {
            var resultado = new OrderProduct();

            try{
                resultado = context.OrderProduct.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(OrderProduct entity)
        {
            throw new System.NotImplementedException();
        }
    }
}