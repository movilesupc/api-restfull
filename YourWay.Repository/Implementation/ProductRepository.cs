using System.Collections.Generic;
using System.Linq;
using YourWay.Entity;
using YourWay.Repository.Context;
using YourWay.Repository.Interface;

namespace YourWay.Repository.Implementation
{
    public class ProductRepository : IProductRepository
    {
        private ApplicationDbContext context;
        public ProductRepository(ApplicationDbContext context){
            this.context = context;
        }


        public bool Create(Product entity)
        {
            try{
                context.Add(entity);
                context.SaveChanges();
            }catch(System.Exception){
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Product> FindAll()
        {
            var resultado = new List<Product>();
            
            try{
                resultado = context.Product.ToList();
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public Product FindById(int id)
        {
            var resultado = new Product();

            try{
                resultado = context.Product.Single(p => p.Id == id);
            }catch(System.Exception){
                throw;
            }
            return resultado;
        }

        public bool Update(Product entity)
        {
            throw new System.NotImplementedException();
        }
    }
}