using YourWay.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace YourWay.Repository.Context
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Account> Account {get; set;}
        public DbSet<Category> Category {get; set;}
        public DbSet<Company> Company {get; set;}
        public DbSet<Competitor> Competitor {get; set;}
        public DbSet<Event> Event {get; set;}
        public DbSet<EventSport> EventSport {get; set;}
        public DbSet<Friend> Friend {get; set;}
        public DbSet<Group> Group {get; set;}
        public DbSet<GroupSport> GroupSport {get; set;}
        public DbSet<Location> Location {get; set;}
        public DbSet<Member> Member {get; set;}
        public DbSet<Order> Order {get; set;}
        public DbSet<OrderProduct> OrderProduct {get; set;}
        public DbSet<Practice> Practice {get; set;}
        public DbSet<Product> Product {get; set;}
        public DbSet<Qualification> Qualification {get; set;}
        public DbSet<Shop> Shop {get; set;}
        public DbSet<Sport> Sport {get; set;}
        public DbSet<Sportsman> Sportsman {get; set;}

        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options){

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder){

            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes()
                .SelectMany(e =>e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}