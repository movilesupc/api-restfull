namespace YourWay.Api
{
    public class SwaggerConfiguration
    {
        public const string EndpointDescription = "YourWay API v1";
        public const string EndpointUrl = "/swagger/v1/swagger.json";
        public const string ContactName = "Team Moviles";
        public const string ContactUrl = "http://moviles-upc.com";
        public const string DocNameV1 = "v1";
        public const string DocInfoTitle = "YourWay API";
        public const string DocInfoVersion = "v1";
        public const string DocInfoDescription = "YourWay Api";
    }
}