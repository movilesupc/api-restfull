using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SportsmanController:ControllerBase
    {
        private ISportsmanService sportsmanService;
        public SportsmanController(ISportsmanService sportsmanService){
            this.sportsmanService = sportsmanService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(sportsmanService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Sportsman sportsman){
            return Ok(sportsmanService.Create(sportsman));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Sportsman sportsman){
            return Ok(sportsmanService.Update(sportsman));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(sportsmanService.FindById(id));
        }
    }
}