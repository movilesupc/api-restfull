using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventSportController:ControllerBase
    {
        private IEventSportService eventSportService;
        public EventSportController(IEventSportService eventSportService){
            this.eventSportService = eventSportService;
        }


        [HttpPost]
        public ActionResult Post([FromBody] EventSport eventSport){
            return Ok(eventSportService.Create(eventSport));
        }
    }
}