using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController:ControllerBase
    {
        private ILocationService locationService;
        public LocationController(ILocationService locationService){
            this.locationService = locationService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(locationService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Location location){
            return Ok(locationService.Create(location));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Location location){
            return Ok(locationService.Update(location));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(locationService.FindById(id));
        }
    }
}