using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompetitorController:ControllerBase
    {
        private ICompetitorService competitorService;
        public CompetitorController(ICompetitorService competitorService){
            this.competitorService = competitorService;
        }


        [HttpPost]
        public ActionResult Post([FromBody] Competitor competitor){
            return Ok(competitorService.Create(competitor));
        }
        
    }
}