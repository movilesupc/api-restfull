using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PracticeController:ControllerBase
    {
        private IPracticeService practiceService;
        public PracticeController(IPracticeService practiceService){
            this.practiceService = practiceService;
        }


        [HttpPost]
        public ActionResult Post([FromBody] Practice practice){
            return Ok(practiceService.Create(practice));
        }
    }
}