using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController:ControllerBase
    {
        private ICategoryService categoryService;
        public CategoryController(ICategoryService categoryService){
            this.categoryService = categoryService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(categoryService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Category category){
            return Ok(categoryService.Create(category));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Category category){
            return Ok(categoryService.Update(category));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(categoryService.FindById(id));
        }
    }
}