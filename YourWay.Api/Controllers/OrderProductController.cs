using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderProductController:ControllerBase
    {
        private IOrderProductService orderProductService;
        public OrderProductController(IOrderProductService orderProductService){
            this.orderProductService = orderProductService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(orderProductService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] OrderProduct orderProduct){
            return Ok(orderProductService.Create(orderProduct));
        }
        [HttpPut]
        public ActionResult Put([FromBody] OrderProduct orderProduct){
            return Ok(orderProductService.Update(orderProduct));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(orderProductService.FindById(id));
        }
    }
}