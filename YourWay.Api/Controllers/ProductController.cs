using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController:ControllerBase
    {
        private IProductService productService;
        public ProductController(IProductService productService){
            this.productService = productService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(productService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Product product){
            return Ok(productService.Create(product));
        }
        [HttpPut]
        public ActionResult Put([FromBody] Product product){
            return Ok(productService.Update(product));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(productService.FindById(id));
        }
    }
}