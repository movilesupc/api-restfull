using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController:ControllerBase
    {
        private IOrderService orderService;
        public OrderController(IOrderService orderService){
            this.orderService = orderService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(orderService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] Order order){
            return Ok(orderService.Create(order));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(orderService.FindById(id));
        }
    }
}