using Microsoft.AspNetCore.Mvc;
using YourWay.Entity;
using YourWay.Service.Interface;

namespace YourWay.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupSportController:ControllerBase
    {
        private IGroupSportService groupSportService;
        public GroupSportController(IGroupSportService groupSportService){
            this.groupSportService = groupSportService;
        }


        [HttpGet]
        public ActionResult Get(){
            return Ok(groupSportService.FindAll());
        }
        [HttpPost]
        public ActionResult Post([FromBody] GroupSport groupSport){
            return Ok(groupSportService.Create(groupSport));
        }
        [HttpPut]
        public ActionResult Put([FromBody] GroupSport groupSport){
            return Ok(groupSportService.Update(groupSport));
        }
        [HttpGet("id")]
        public ActionResult Get(int id){
            return Ok(groupSportService.FindById(id));
        }
    }
}